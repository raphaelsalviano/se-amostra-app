package br.com.ufpb.seamostra.model;

import java.io.Serializable;

public class Event implements Serializable {

    private int _id;
    private int day;
    private int idImage;
    private String type;
    private String title;
    private String people;
    private String hour;

    private String description;
    private String sizeLimit;
    private String local;

    public Event() {
    }

    public Event(int day, int idImage, String type, String title, String people, String hour, String description, String sizeLimit) {
        this.day = day;
        this.idImage = idImage;
        this.type = type;
        this.title = title;
        this.people = people;
        this.hour = hour;
        this.description = description;
        this.sizeLimit = sizeLimit;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getIdImage() {
        return idImage;
    }

    public void setIdImage(int idImage) {
        this.idImage = idImage;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPeople() {
        return people;
    }

    public void setPeople(String people) {
        this.people = people;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSizeLimit() {
        return sizeLimit;
    }

    public void setSizeLimit(String sizeLimit) {
        this.sizeLimit = sizeLimit;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }
}
