package br.com.ufpb.seamostra.util.fonts;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.TextView;

public class RobotoLightTypeface extends TextView {

    public RobotoLightTypeface(Context context) {
        super(context);
        setTypefaceRoboto(context);
    }

    public RobotoLightTypeface(Context context, AttributeSet attrs) {
        super(context, attrs);
        setTypefaceRoboto(context);
    }

    public RobotoLightTypeface(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setTypefaceRoboto(context);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public RobotoLightTypeface(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        setTypefaceRoboto(context);
    }

    private void setTypefaceRoboto(Context context){
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "fonts/roboto_light.ttf");
        setTypeface(typeface);
    }
}
