package br.com.ufpb.seamostra.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;

import com.squareup.picasso.Picasso;

import br.com.ufpb.seamostra.R;
import br.com.ufpb.seamostra.util.custom.CircleTransform;

public class AboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_about);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        String[] array = new String[]{"Contate a equipe do Se Amostra", "Contate ou conheça os desenvovledores"};

        ListView listView = (ListView) findViewById(R.id.listView);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, array);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                if(position == 0){
                    View view1 = View.inflate(AboutActivity.this, R.layout.alert_about_event, null);
                    Picasso.with(AboutActivity.this).load(R.drawable.ico1).transform(new CircleTransform()).into(((ImageView)view1.findViewById(R.id.fb)));
                    Picasso.with(AboutActivity.this).load(R.drawable.ico_insta).transform(new CircleTransform()).into(((ImageView)view1.findViewById(R.id.insta)));
                    Picasso.with(AboutActivity.this).load(R.drawable.ico_snap).transform(new CircleTransform()).into(((ImageView)view1.findViewById(R.id.snapchat)));
                    new AlertDialog.Builder(AboutActivity.this)
                            .setTitle("Contate-nos")
                            .setView(view1)
                            .create().show();
                }else{
                    View view2 = View.inflate(AboutActivity.this, R.layout.alert_about_developer, null);
                    new AlertDialog.Builder(AboutActivity.this)
                            .setTitle("Desenvolvido por:")
                            .setView(view2)
                            .create().show();
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        int id = item.getItemId();

        if(id == android.R.id.home){
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }

        return super.onContextItemSelected(item);
    }
}
