package br.com.ufpb.seamostra.util.custom;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import br.com.ufpb.seamostra.R;
import br.com.ufpb.seamostra.model.Comissao;

public class ComissaoAdapter extends RecyclerView.Adapter<ComissaoAdapter.EventsViewHolder> {

    private Context context;
    private List<Comissao> comissaos;
    private LayoutInflater layoutInflater;

    public ComissaoAdapter(Context context, List<Comissao> comissaos) {
        this.context = context;
        this.comissaos = comissaos;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public EventsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_comissao_main, parent, false);
        return new EventsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(EventsViewHolder holder, int position) {
        Comissao comissao = comissaos.get(position);

        holder.mImage.setImageDrawable(context.getResources().getDrawable(comissao.getIdImage()));
        holder.mTitle.setText(comissao.getName());
    }

    @Override
    public int getItemCount() {
        return comissaos.size();
    }

    public class EventsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private View mClick;
        private ImageView mImage;
        private TextView mTitle;

        public EventsViewHolder(View itemView) {
            super(itemView);

            mClick = itemView.findViewById(R.id.card_main);
            mClick.setOnClickListener(this);

            mImage = (ImageView) itemView.findViewById(R.id.image_main);
            mTitle = (TextView) itemView.findViewById(R.id.name_card_main);
        }

        @Override
        public void onClick(View view) {
            Comissao comissao = comissaos.get(getPosition());
            View view1 = layoutInflater.inflate(R.layout.alert_view, null, false);
            ((ImageView) view1.findViewById(R.id.imagePeople)).setImageDrawable(context.getResources().getDrawable(comissao.getIdImage()));
            ((TextView) view1.findViewById(R.id.namePeople)).setText(comissao.getName());
            ((TextView) view1.findViewById(R.id.status_people)).setText(comissao.getStatus());
            ((TextView) view1.findViewById(R.id.description_people)).setText(comissao.getDescription());
            new AlertDialog.Builder(context)
                    .setView(view1)
                    .setPositiveButton("OK", null)
                    .create().show();
        }
    }

}
