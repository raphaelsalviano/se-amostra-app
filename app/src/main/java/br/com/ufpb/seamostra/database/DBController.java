package br.com.ufpb.seamostra.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import br.com.ufpb.seamostra.model.Event;

/**
 * Created by rapha on 14/05/2016.
 */
public class DBController {

    private DBCore core;
    private SQLiteDatabase database;

    public DBController(Context context){
        core = new DBCore(context);
    }

    public List<Event> getEvetsDay(int day){
        database = core.getReadableDatabase();
        List<Event> events = new ArrayList<>();

        String[] colluns = new String[]{"_id", "day", "idImage", "type", "title",
                "people", "hour", "description", "sizeLimit", "local"};
        Cursor cursor = database.query("event", colluns, "day = ?", new String[]{""+day}, null, null, null);
        if(cursor.getCount() > 0){
            cursor.moveToFirst();
            do{
                Event event = new Event();
                event.set_id(cursor.getInt(0));
                event.setDay(cursor.getInt(1));
                event.setIdImage(cursor.getInt(2));
                event.setType(cursor.getString(3) != null ? cursor.getString(3) : " ");
                event.setTitle(cursor.getString(4) != null ? cursor.getString(4) : " ");
                event.setPeople(cursor.getString(5) != null ? cursor.getString(5) : " ");
                event.setHour(cursor.getString(6) != null ? cursor.getString(6) : " ");
                event.setDescription(cursor.getString(7) != null ? cursor.getString(7) : " ");
                event.setSizeLimit(cursor.getString(8) != null ? cursor.getString(8) : " ");
                event.setLocal(cursor.getString(9) != null ? cursor.getString(9) : " ");

                events.add(event);
            }while (cursor.moveToNext());
        }

        return events;
    }
}
