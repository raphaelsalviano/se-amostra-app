package br.com.ufpb.seamostra.database;

import java.util.ArrayList;
import java.util.List;

public class DBCode {

    public List<String> createTables() {

        List<String> tables = new ArrayList<>();

        tables.add("event (_id integer primary key autoincrement," +
                "day integer not null, idImage integer, type text," +
                "title text, people text, hour varchar," +
                "description text, sizeLimit text, local text)");

        return tables;

    }

    public List<String> insertsTable() {

        List<String> tables = new ArrayList<>();
        // 17
        tables.add("event (day, title, people, hour, local) VALUES (17, 'Credenciamento', 'Se Amostra 2016', '14h às 18h', 'CA de Design')");
        tables.add("event (day, title, people, hour, local) VALUES (17, 'Abertura', 'Se Amostra 2016', '19h', 'Central de Aulas')");
        tables.add("event (day, title, people, hour, local) VALUES (17, 'Repentina', 'Se Amostra 2016', '20h às 22h', 'A ser definido')");
        // 18
        tables.add("event (day, type, title, people, hour, description, sizeLimit, local) " +
                "VALUES (18, 'Oficina', 'VestiDesign', 'Irandir', '08h às 12h'," +
                " 'Criatividade, sustentabilidade, moda e ousadia. \n" +
                "Se você tem peças de roupas em seu armário e não sabe como utilizá-las, por ser um modelo passado, fora das tendências da moda, venha pra VestiDesign, a oficina que vai lhe ensinar a customizar suas peças e dar um UP no seu jeito de se vestir, vista design e arrase por aí. \n" +
                "Customizações: Cortes. Aplicações. Pinturas. Montagem de uma roupa de papel.', '15', 'Sala RB-102')");

        tables.add("event (day, type, title, people, hour, description, sizeLimit, local) " +
                "VALUES (18, 'Oficina', 'Rolinhos Decorados', 'Rayssa', '08h às 12h'," +
                " 'E quem nunca quis por a mão na massa e fazer aquela moldura bem fofinha pro espelho do quarto ou aquele quadrinho maneirinho pra dar a um amigo?\n" +
                "Pois, pensando nisso resolvemos juntar o útil ao agradável e reciclar os bons e velhos rolinhos de papel para fazer molduras e quadrinhos decorativos...\n" +
                "Quer aprender? é só vir se amostrar na nossa oficina', 'Livre', 'Sala RA-101')");

        tables.add("event (day, type, title, people, hour, description, sizeLimit, local) " +
                "VALUES (18, 'Palestra', 'Show me the money!', 'Rodrigo Rebouças', '10h às 12h'," +
                " 'O seu produto ou serviço gera valor para o seu cliente? Aprenda a criar produtos e serviços que os clientes desejam!', 'Livre', 'Sala RA-106')");

        tables.add("event (day, type, title, people, hour, description, sizeLimit, local) " +
                "VALUES (18, 'Oficina', 'Do fio à imaginação: criando jóias em tecido', 'Joey Almeida', '08h às 12h'," +
                " 'Este minicurso possibilita o uso de aviamentos e tecidos na confecção de joias. A técnica de enrolar linhas em base fixa, como o cordão de algodão é antiga e ancestral, portanto valoriza o que possuímos: a técnica ancestral, a criatividade, materiais acessíveis e um olhar curioso para possibilidades infinitas de criar joias e outros produtos. \n" +
                "É a economia criativa valorizando o que temos de melhor no design!'," +
                " 'Livre', 'Sala RF-101:Desenho')");

        tables.add("event (day, type, title, people, hour, description, sizeLimit, local) " +
                "VALUES (18, 'Oficina', 'Ilustra BOMB', 'Chan', '09h às 11h'," +
                " 'Chamada: Acha bacana as tipografias do grafitti e os bombs (grafitti rápido) mas nunca se arriscou a fazer? Vamos construir no papel… depois… aí é com vocês!', '20 pessoas', 'Sala RA-102')");

        tables.add("event (day, title, people, hour, local) VALUES (18, 'Almoço', 'Se Amostra 2016', '12h às 14h', 'RU - Rio Tinto')");

        tables.add("event (day, type, title, people, hour, description, sizeLimit, local) " +
                "VALUES (18, 'Oficina', 'Aquarela para iniciantes: representatividade e estudos sobre a pele negra', 'Silvelena', '14h às 15:30h'," +
                " 'A publicitária Silvelena Gomes vem lá da Terra da Luz pra falar um pouco sobre sua luta em defesa das mulheres e da comunidade negra, apresentar seu projeto pessoal e seu amor pela tipografia e pela aquarela, palestrando sobre conceitos iniciais da pintura e propondo um debate construtivo sobre questões cada dia mais importantes na nossa sociedade. Se você aprecia a arte e acredita num mundo mais bonito por meio dela, essa é a sua oficina.'," +
                " '20 pessoas', 'Sala RA-102')");

        tables.add("event (day, type, title, people, hour, description, sizeLimit, local) " +
                "VALUES (18, 'Palestra', 'Relações sustentáveis entre pessoas e marcas', 'Bruno Lucena', '15:30h às 17h'," +
                " 'As marcas perderam – ou nunca desenvolveram – a habilidade de se relacionarem intimamente com as pessoas. Buscando construir laços de maneira autêntica e amorosa, sem invadir nosso espaço, explorar e olhar para nós apenas como indivíduos que existimos para Consumir. \n" +
                "Os novos contextos empresariais, no qual as marcas protagonizam as cenas, influenciando nossos comportamento e até valores, exigem uma nova forma de visão e gestão sobre os negócios.\n" +
                "Para iniciar este processo de conexão com o futuro, as marcas devem antes de tudo entender que precisam ser verdadeiras, éticas e inovadoras. Tendo o Branding e o Design como filosofias e metodologias fundamentais para gerar ideias, criar e conduzí-las nestes novos cenários.', 'Livre', 'Sala RA-106')");

        tables.add("event (day, type, title, people, hour, description, sizeLimit, local) " +
                "VALUES (18, 'Oficina', 'VIDEOMAKER: dicas e truques', 'Mateus Martins', '14h às 17h'," +
                " 'O YouTube é uma plataforma incrível, onde o próprio usuário cria o conteúdo da mesma. A produção audiovisual de forma amadora vem crescendo exponencialmente, e pensando nisso essa oficina oferecerá dicas e truques para trazer um aspecto profissional aos seus vídeos e com baixo custo. Efeitos digitais, iluminação, monetização, direitos autorais, som e muito mais!'," +
                " '30 pessoas', 'Sala RC-103:Lab. Informática')");

        tables.add("event (day, type, title, people, hour, description, sizeLimit, local) " +
                "VALUES (18,  'Palestra', 'O papel do design sociocultural - Criação e Imaginação', 'Chan', '14h às 16h'," +
                " 'A temática aborda um passeio sobre o que é o design social e cultural, com base numa visão de elaboração de projetos pra uma sociedade em específico, e em projetos voltados a essa cultura, e a relação do ensino do design e sua prática. A ideia é trazer a responsabilidade de nós, designers, e estimular a criatividade de se pensar em projetos (gráficos ou de produto), exemplificando com situações de uma cultura própria, e também através de questionamentos e dinâmicas.'," +
                " '40 pessoas', 'Sala RF-101:Desenho')");

        tables.add("event (day, type, title, people, hour, description, sizeLimit, local) " +
                "VALUES (18, 'Palestra', 'Moda, design e inclusão social no programa mulheres mil', 'Joey Almeida', '16h às 17h'," +
                " 'Este minicurso possibilita o uso de aviamentos e tecidos na confecção de joias. A técnica de enrolar linhas em base fixa, como o cordão de algodão é antiga e ancestral, portanto valoriza o que possuímos: a técnica ancestral, a criatividade, materiais acessíveis e um olhar curioso para possibilidades infinitas de criar joias e outros produtos. \n" +
                "É a economia criativa valorizando o que temos de melhor no design!'," +
                " '15 pessoas', 'Sala RA-205:Segundo piso')");

        tables.add("event (day, type, title, people, hour, description, sizeLimit, local) " +
                "VALUES (18, 'Oficina', 'Conceitos básicos do graffit', 'Jota Zero', '14h às 17h'," +
                " 'Nessa oficina você vai poder conhecer o material usado para a criação do grafite, a execução de técnicas básicas e também as artimanhas de uso do material para se obter um bom desempenho.'," +
                " '20 pessoas', 'Praça dos Ventos')");

        tables.add("event (day, type, title, people, hour, description, sizeLimit, local) " +
                "VALUES (18, 'Oficina', 'Projeto de interface para aplicação móvel', 'Yuska Aguiar', '14h às 17h'," +
                " 'Projeto de interface para aplicação móvel tomando como base o uso de técnicas de User Experience (UX) como: brainstorming, proto-personas para representar usuários potenciais, card sorting para organização das funções da aplicação e prototipagem em papel simulada com aplicativos (POP/Marvel)" +
                "\nNão esqueça de baixar os aplicativos:\n- POP para Android;\n- Marvel para iOS\n'," +
                " '20 pessoas', 'Sala RA-101')");

        tables.add("event (day, title, people, hour, local) VALUES (18, 'Jantar', 'Se Amostra 2016', '18h às 20h', 'RU - Rio Tinto')");

        tables.add("event (day, title, people, hour) VALUES (18, 'Intervenções', 'Se Amostra 2016', '20h às 22h')");

        tables.add("event (day, title, people, hour, local) VALUES (18, 'Festa: Subúrbia', 'Se Amostra 2016', 'Inicio às 22h', 'Ginásio Gerbasão - Rio Tinto')");

        //19

        tables.add("event (day, type, title, people, hour, description, sizeLimit, local) " +
                "VALUES (19, 'Oficina', 'Visita monitorada a pontos selecionados da cidade-fábrica de rio tinto com exercício de desenho de observação', 'Luciene Lehmkuhl', '08:30h às 11:30h'," +
                " 'Esta atividade é uma iniciativa do projeto Referências urbanas de Rio Tinto para o desenvolvimento de projetos de produtos em Design em parceria com o Se Amostra 2016.', '30', 'Em frente à Oca')");

        tables.add("event (day, type, title, people, hour, description, sizeLimit, local) " +
                "VALUES (19, 'Oficina', 'Lambes lambem a pele dos muros – street art por colagem de rua', 'Hêvilla Costa', '09h às 12h'," +
                " 'A seguinte oficina propõe um encontro de compartilhamento de ideias, a fim de apresentar um contexto teórico breve da realidade e essência desse tipo de manifestação urbana, surgimento, causas, intenções e tipos e técnicas de produção, focando principalmente na arte do último século como influenciador desse contexto criativo dos lambes, como base introdutória dos participantes ao universo da street art para uma breve discussão e debate que antecede a produção prática da oficina'," +
                " '15', 'Sala RF-101:Desenho')");

        tables.add("event (day, type, title, people, hour, description, sizeLimit, local) " +
                "VALUES (19, 'Palestra', 'DEDOVERDE: desenvolvimento de identidade (visual também) e processos criativos', 'Wanessa', '11h às 12h'," +
                " 'Tempo, estudo e auto percepção. 11 anos de nado livre em arte. Influências, nome artístico, identidade visual e processos (em graffiti, ilustração e tatuagem)'," +
                " 'Livre', 'Sala RA-106')");

        tables.add("event (day, type, title, people, hour, description, sizeLimit, local) " +
                "VALUES (19, 'Oficina', 'Mini-jardins caseiros ecológicos / transformando lixo em vasos geométricos', 'Tássia Costa', '08:30h às 12h'," +
                " 'Usando o conhecimento acerca de planificação de sólidos, será utilizado lixo doméstico para a produção de vasos geométricos minimalistas, para plantas de pequeno porte (apartamento), como cactos e suculentas. A oficina pretende demonstrar mais de uma técnica e incentiva o uso de mais de um material como matéria prima, porém o enfoque será em caixas tetrapak de leite e suco de 1 litro para a produção dos vasos geométricos.\n" +
                "Do ponto de vista teórico, a oficina abordará os temas: design sustentável, geometria/planificação e artesania. Além de uma pincelada sobre design de interiores.'," +
                " '12', 'Sala RA-102')");

        tables.add("event (day, type, title, people, hour, description, local) " +
                "VALUES (19, 'Intervenção', 'Intervenção', 'Zello', '08h às 12h'," +
                " 'Interferência de uma maneira criativa e original. Um Totem? Um Monumento? Um Parlatório? Um objeto que vai ter voz, com uso da colagem - cortes e recortes, cores e traços - impressões!\n" +
                "A comunhão com arte, ser e estar, diante do inusitado! Vamos nos \"mostrar\" com letras da poesia, intuitivamente desenhar sonhos, brincar de ser criança!\n" +
                "Seremos um só...naquele objeto. O grito no sussurro do silencio, que pode ser...todas as cores nas formas impressas no papel, o nosso papel, de artistas...do Designer!', 'Praça dos Ventos')");

        tables.add("event (day, title, people, hour, local) VALUES (19, 'Almoço', 'Se Amostra 2016', '12h às 14h', 'RU - Rio Tinto')");

        tables.add("event (day,  type, title, people, hour, description, sizeLimit, local) " +
                "VALUES (19, 'Workshop', 'O design além do designer', 'Sabiá', '14h às 18h'," +
                " 'O intuito do workshop é trazer a experiência de trabalho colaborativo e gestão estratégica do Design para desenvolver o senso crítico de cada participante e evidenciar que o Design vai além da execução, podendo ser gestor de inovação e solucionar demandas reais da sociedade.'," +
                " '20 pessoas', 'Sala RA-101')");

        tables.add("event (day, type, title, people, hour, description, sizeLimit, local) " +
                "VALUES (19, 'Oficina', 'Paredes Art Zona', 'Luiz Ricardo Sales', '14h às 17h'," +
                " 'A oficina tem por objetivo proporcionar o aprendizado e o desenvolvimento de afrescos feitos através do graffitti. Com a utilização de diversas técnicas que facilitem a execução das intervenções artísticas. Os desenhos a serem desenvolvidos envolveram as misturas de cores e tonalidades, caligrafia e temas do cotidiano, além da expressão criativa do participante.'," +
                " '20 pessoas', 'Sala RA-102')");

        tables.add("event (day, type, title, people, hour, description, sizeLimit, local) " +
                "VALUES (19, 'Palestra', 'Como fazer apresentações inteligentes', 'Eduardo Lima', '14h às 15:30h'," +
                " 'Como apresentar um produto ou uma ideia de forma inteligente e interessante garantindo mais atenção de sua audiência e aumentando o engajamento. Um bate papo filosófico e cheio de dicas e macetes de como preparar apresentações que tragam resultados mais efetivos'," +
                " '40 pessoas', 'Sala RA-105:Segundo piso')");

        tables.add("event (day, type, title, people, hour, description, sizeLimit, local) " +
                "VALUES (19, 'Mesa Redonda', 'Possibilidade de uso das tecnologias de realidade virtual e aumentada em design', 'Angélica, Anthony e Ana Priscila', '15:30h às 17:30h'," +
                " 'Nessa mesa redonda vamos conversar sobre as tecnologias emergentes, como a Realidade Virtual e Aumentada e suas possibilidades de uso em pesquisas e projetos em Design. O que já é possível? qual o futuro?'," +
                " 'Livre', 'Sala RA-106')");

        tables.add("event (day, type, title, people, hour, description) " +
                "VALUES (19, 'Intervenção', 'Intervenção', 'Zello', '14h às 18h'," +
                " 'Interferência de uma maneira criativa e original. Um Totem? Um Monumento? Um Parlatório? Um objeto que vai ter voz, com uso da colagem - cortes e recortes, cores e traços - impressões!\n" +
                "A comunhão com arte, ser e estar, diante do inusitado! Vamos nos \"mostrar\" com letras da poesia, intuitivamente desenhar sonhos, brincar de ser criança!\n" +
                "Seremos um só...naquele objeto. O grito no sussurro do silencio, que pode ser...todas as cores nas formas impressas no papel, o nosso papel, de artistas...do Designer!')");

        tables.add("event (day, title, people, hour, local) VALUES (19, 'Jantar', 'Se Amostra 2016', '18h às 20h', 'RU - Rio Tinto')");

        //20
        tables.add("event (day, type, title, people, hour, description, local) " +
                "VALUES (20, 'Oficina', 'Sinais de Design em Libras no Brasil', 'Everton Borba', '08h às 12h'," +
                " 'É muito difícil encontrar um espaço em que se ensine libras no Brasil, e a comunidade surda sente o impacto que isso causa. Com a intenção de mostrar que libras pode ser interessante para qualquer um, esta oficina mistura libras às atividades presentes Design, ensinando ambos de uma maneira divertida.', 'Sala RA-102')");

        tables.add("event (day, type, title, people, hour, description, sizeLimit, local) " +
                "VALUES (20, 'Workshop', 'Quando o básico é tudo que se precisa! Como noções básicas de fotografia, aguçamento do olhar e criatividade podem transformar suas fotos - para melhor!', 'Ricardo Pinto', '08h às 10h'," +
                " 'Esta é a ideia do workshop básico de fotografia, com Ricardo Pinto, tecnólogo em vídeo digital do DEMID/UFPB.'," +
                " '20 pessoas', 'Sala RB-102:Fotografia')");

        tables.add("event (day, type, title, people, hour, description, sizeLimit, local) " +
                "VALUES (20, 'Palestra', 'GRAFFITI E PICHAÇÃO: os dois lados que atuam no meio urbano', 'Luiz Ricardo Sales', '10h às 11h'," +
                " 'A palestra abordará considerações sobre a história do grafite e como este difere da pichação, partindo de uma análise concisa de alguns autores e obras de artistas que fazem uso dessa forma de expressão.'," +
                " 'Livre', 'Sala RA-106')");

        tables.add("event (day, type, title, people, hour, description, sizeLimit, local) " +
                "VALUES (20, 'Palestra', 'Se amostrando para diferentes classes sociais', 'Ari Junior', '11h às 12h'," +
                " 'Qual a linguagem do seu público alvo? Todos estão online, mas nem todos vão entender a sua mensagem.'," +
                " 'Livre', 'Sala RA-205:Segundo piso')");

        tables.add("event (day, type, title, people, hour, description, sizeLimit, local) " +
                "VALUES (20, 'Mesa Redonda', 'ARTE URBANA ATIVISTA – Ações coletivas e poéticas', 'Hêvilla, Priscila e Wanessa', '09h às 12h'," +
                " 'Tem como proposta relatar a experiência pessoal da mulher e do discurso de gênero, políticas públicas femininas e defesa e emponderamento a mulher no espaço urbano, por meio de atividades de street art. Através do depoimento e conversa perante as atividades que as convidadas desenvolvem e/ou pesquisam, afim de discutir e refletir sobre essa plataforma de luta.'," +
                " 'Livre', 'Sala RA-101')");

        tables.add("event (day, type, title, people, hour, description, sizeLimit, local) " +
                "VALUES (20, 'Palestra', 'Os desígnios do design', 'Vant Vaz', '11:30h às 12h'," +
                " 'Estamos presenciando um período e vivendo cada vez mais em uma sociedade espetacularizada predominantemente visual que se move em velocidade avassaladora. A visualidade está em quase tudo, em toda parte nas paisagens urbanas. Das formas, cores e aspectos e da necessidade constante da inovação e utilidade, as coisas são descartáveis e efêmeras, tudo é consumível e logo se tornam obsoletas na realidade mutável e mutante em que vivemos. O tempo do “ver”, assim como do “estar” possivelmente negando o tempo do “ser” nesta era da informação e da imagem.\n" +
                "Há muito que se entender disso tudo. Das moventes ondas do novo que apontam saídas aos ardis que nossas culturas engendram habitando o mesmo espaço “líquido” ou “virtual” das interações e relações humanas referendando o olhar do sociólogo polaco Zygmunt Bauman lançado na modernidade, citando um dos pensadores que se dedicam ao escrutínio e zeitgeist de nosso tempo. \n" +
                "Se levamos uma vida para o consumo que termina desaguando no consumo da vida, seria possível reverter a forma como concebemos o mundo e reservar mais tempo para acolher as conexões que fazemos? De quanto tempo precisaríamos para perceber os meandros da sociedade visual e utilitária em que vivemos para salvaguardar nossa saúde cultural e permitir a convivência do permanente ou duradouro com o efêmero? Esta via ruidosa e vertiginosa seria irreversível e inevitável, parte de nossa natureza que reverbera o desejo de obter sempre mais e mais na mesma velocidade que descartamos as coisas mesmo sob o preço da destruição de tudo que nos cerca? A visualidade, a estética, a formatação, a utilidade e praticidade, qual o propósito do design deste tempo e qual sua ética? Aqui proponho algumas questões para uma conversa dialógica sobre o assunto e pensarmos mais sobre o propósito de quase todo empreendimento humano que deveria ser a manutenção de nossa própria humanidade e descortinarmos, caso seja possível, poéticas visuais mais humanas.'," +
                " 'Livre', 'Bloco A')");

        tables.add("event (day, type, title, people, hour, description) " +
                "VALUES (20, 'Intervenção', 'Intervenção', 'Zello', '08h às 12h'," +
                " 'Interferência de uma maneira criativa e original. Um Totem? Um Monumento? Um Parlatório? Um objeto que vai ter voz, com uso da colagem - cortes e recortes, cores e traços - impressões!\n" +
                "A comunhão com arte, ser e estar, diante do inusitado! Vamos nos \"mostrar\" com letras da poesia, intuitivamente desenhar sonhos, brincar de ser criança!\n" +
                "Seremos um só...naquele objeto. O grito no sussurro do silencio, que pode ser...todas as cores nas formas impressas no papel, o nosso papel, de artistas...do Designer!')");

        tables.add("event (day, title, people, hour, local) VALUES (20, 'Almoço', 'Se Amostra 2016', '12h às 14h', 'RU - Rio Tinto')");

        tables.add("event (day, type, title, people, hour, description, sizeLimit, local) " +
                "VALUES (20, 'Oficina', 'Paredes Art Zona', 'Luiz Ricardo', '14h às 17h'," +
                " 'A oficina tem por objetivo proporcionar o aprendizado e o desenvolvimento de afrescos feitos através do graffitti. Com a utilização de diversas técnicas que facilitem a execução das intervenções artísticas. Os desenhos a serem desenvolvidos envolveram as misturas de cores e tonalidades, caligrafia e temas do cotidiano, além da expressão criativa do participante.'," +
                " '20 pessoas', 'Sala RA-102')");

        tables.add("event (day, type, title, people, hour, description, sizeLimit, local) " +
                "VALUES (20, 'Oficina', 'Tipografia Urbana (letras utilizadas em graffiti e assinaturas - as tags)', 'Priscila Lima', '14h às 17h'," +
                " 'A oficina vai abordar, analisar e pôr em prática a utilização da tipografia urbana do graffiti e da pichação, e como cada tipo de letra indica forte ligação com a cultura local, bem como a tipografia vernacular.'," +
                " '15 pessoas', 'Sala RA-101')");

        tables.add("event (day, type, title, people, hour, description,sizeLimit, local) " +
                "VALUES (20, 'Apresentação', 'Apresentação', 'Vant Vaz', '14h às 14:30h'," +
                " 'Apresentação','Livre', 'na Oca')");

        tables.add("event (day, type, title, people, hour, description, sizeLimit, local) " +
                "VALUES (20, 'Workshop', 'Os detalhes fazem a diferença (Photoshop)', 'Ari Junior', '14:30h às 16h'," +
                " 'Como melhorar sua imagem antes de entregar para o cliente.'," +
                " '30 pessoas', 'Sala RB-103:Lab. Informática')");

        tables.add("event (day, type, title, people, hour, description, sizeLimit, local) " +
                "VALUES (20, 'Palestra', 'O design nos quadrinhos', 'Thais Gualberto', '16h às 17h'," +
                " 'Essa palestra abordará as diversas facetas do design nos quadrinhos, desde a ilustração até a diagramação do livro.'," +
                " 'Livre', 'Sala RA-106')");

        tables.add("event (day,type, title, people, hour, description, sizeLimit, local) " +
                "VALUES (20, 'Palestra', 'Biolearning: uma face do design e inovação', 'BI/OS', '14h às 18h'," +
                " 'Considera-se que é natural se inspirar na natureza na conformação de novos artefatos. É observado que existe inteligência com larga capacidade em diversos setores que ainda não alcançamos tamanho desenvolvimento - materiais, processos, relações sociais, ecologia, estruturas, ciclo de vida, entre tantos outros. Portanto, podemos aprender com a Natureza. Assim, aponta-se sua viabilidade por meio de tecnologias computacionais - design paramétrico e fabricação digital - é possível gerar inovação em Design e Arquitetura, a partir de artefatos e estratégias biodigitais, tal como mediadores para as sociedades, tornando-se uma nova face do design, quebrando paradigmas estéticos e funcionais. E o melhor, é possível fazer tudo isso com tecnologia que já são acessíveis em nossa região.'," +
                " 'Livre', 'Bloco A:Central')");

        tables.add("event (day, title, people, hour, local) VALUES (20, 'Jantar', 'Se Amostra 2016', '18h às 20h', 'RU - Rio Tinto')");

        tables.add("event (day, title, people, hour, local) VALUES (20, 'Festa: Alice', 'Se Amostra 2016', 'Á partir das 22h', 'Ginásio Gerbasão - Rio Tinto')");

        //21
        tables.add("event (day, type, title, people, hour, description, sizeLimit, local) " +
                "VALUES (21, 'Oficina', 'Desenvolvendo websites com o Adobe Muse', 'Raphael Salviano e Ryslânia Santos', '08h às 12h'," +
                " 'Quem nunca quis produzir seu próprio site sem utilizar códigos ou depender de templates pré-definidos? Não adianta fingir, todos nós - ou menos a maioria, já pensou em fazer isso. Apesar do layout estar finalizado em nossa cabeça, o processo de desenvolvimento de um site envolve um pouco de programação e isso nem sempre é tão simples, concorda?! Pensando nisso, que tal aprender uma ferramenta que é capaz de criar sites - do mesmo jeitinho que você imaginou, sem precisar utilizar uma linha de código para isso? Vem pra essa oficina, e vamos começar os trabalhos!'," +
                " '30 pessoas', 'Sala RB-103:Lab. Informática')");

        tables.add("event (day, type, title, people, hour, description, sizeLimit, local) " +
                "VALUES (21, 'Oficina', 'Turbante-se', 'Jéssica Menezes', '08:30h às 12h'," +
                " 'Turbante-se, oficina de turbante e box braids: mais que tecido na cabeça, de coroas a acessórios urbanos.'," +
                " 'Livre', 'Sala RA-102')");

        tables.add("event (day, type, title, people, hour, description, sizeLimit, local) " +
                "VALUES (21, 'Extac', 'Exposição de trabalhos acadêmicos', 'Extac', '09h às 12h'," +
                " 'Vem olhar a exposição de trabalhos acadêmicos'," +
                " 'Livre', 'Sala RA-106')");

        tables.add("event (day, type, title, people, hour, description, sizeLimit, local) " +
                "VALUES (21, 'Wordkshop', 'Fotografia publicitária no meio musical', 'Steven Ellisson', '10h às 11:30h'," +
                " 'Workshop relacionado à fotografia publicitária, mostrando a vivência, quais equipamentos utilizar, resultados e como atingir aos mesmos resultados de forma dinâmica e envolvente.'," +
                " '30 pessoas', 'Sala RA-205:Segundo piso')");

        tables.add("event (day, title, people, hour, local) VALUES (21, 'Almoço', 'Se Amostra 2016', '12h às 14h', 'RU - Rio Tinto')");

        tables.add("event (day, title, people, hour, sizeLimit) " +
                "VALUES (21, 'Degrau', 'Degrau', '10h às 11:30h'," +
                " 'Livre')");

        tables.add("event (day, type, title, people, hour, description, sizeLimit, local) " +
                "VALUES (21, 'Oficina', 'Transforme sua ideia mirabolante em um negócio \"amostrado\" em 9 passos', 'Rafael Magalhães', '14h às 18h'," +
                " 'A oficina permitirá que os participantes transformem suas ideias mirabolantes em um modelo de negócios mais tangível, extremamente prático e factível. Através do Canvas do Business Model Generation, uma ferramenta mundialmente conhecida por startups e utilizada até pelo Sebrae, os participantes aprenderão a modelar seus negócios através de nove (9) etapas fundamentais que incluem o valor proposto, o foco no consumidor, formas de entrega e apresentação, finanças, dentre outros. Tudo isso a partir de exemplos concretos e ligados às inovações que só os que \"Se Amostram\" são capazes de realizar.'," +
                " '30 pessoas', 'Sala RA-101')");

        tables.add("event (day, type, title, people, hour, description, sizeLimit, local) " +
                "VALUES (21,'Mesa Redonda', 'Mesa Redonda com Elabora', 'Elabora', '14h às 18h'," +
                "'A mesa redonda tem o intuito discutir sobre o movimento empresarial júnior e como é o funcionamento dás empresas juniores dentro da universidade, e como isso influência no seu desempenho no mercado de trabalho.'," +
                " 'Livre', 'Sala RA-106')");

        tables.add("event (day, type, title, people, hour, sizeLimit, local) " +
                "VALUES (21, 'Oficina', 'Usando o Scketchup e V-Ray', 'Se Amostra 2016', '14h às 18h'," +
                " '30 pessoas', 'Sala RB-103:Lab. Informática')");

        tables.add("event (day, type, title, people, hour, sizeLimit) " +
                "VALUES (21, 'Desfile', 'Desfile de encerramento', 'Se Amostra 2016', '17h às 18h'," +
                " 'Livre')");

        tables.add("event (day,title, people, hour) VALUES (21, 'Janta', 'Se Amostra 2016', '18h às 20h')");

        //22
        tables.add("event (day,title, people, hour) VALUES (22, 'Desmonte das barracas', 'Se Amostra 2016', 'Dia todo')");

        return tables;

    }


}
