package br.com.ufpb.seamostra.model;

import java.io.Serializable;

/**
 * Created by rapha on 14/05/2016.
 */
public class Comissao implements Serializable {

    private int idImage;
    private String name;
    private String description;
    private String status;

    public Comissao(int idImage, String name, String description, String status) {
        this.idImage = idImage;
        this.name = name;
        this.description = description;
        this.status = status;
    }

    public int getIdImage() {
        return idImage;
    }

    public void setIdImage(int idImage) {
        this.idImage = idImage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
