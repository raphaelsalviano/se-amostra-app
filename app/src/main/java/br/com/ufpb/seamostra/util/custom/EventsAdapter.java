package br.com.ufpb.seamostra.util.custom;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import br.com.ufpb.seamostra.R;
import br.com.ufpb.seamostra.model.Event;

public class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.EventsViewHolder> {

    private Context context;
    private List<Event> events;
    private LayoutInflater layoutInflater;

    public EventsAdapter(Context context, List<Event> events) {
        this.context = context;
        this.events = events;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public EventsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_list_frag, parent, false);
        return new EventsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(EventsViewHolder holder, int position) {

        Event event = events.get(position);

        Picasso.with(context).load(R.drawable.ico1).transform(new CircleTransform()).into(holder.mImage);
        holder.mType.setText("" + event.getType());
        holder.mTitle.setText("" + event.getTitle());
        holder.mPeopleName.setText("" + event.getPeople());
        holder.mHour.setText("" + event.getHour());

    }

    @Override
    public int getItemCount() {
        return events.size();
    }

    public class EventsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private View mClick;
        private ImageView mImage;
        private TextView mType;
        private TextView mTitle;
        private TextView mPeopleName;
        private TextView mHour;

        public EventsViewHolder(View itemView) {
            super(itemView);

            mClick = itemView.findViewById(R.id.click);
            mClick.setOnClickListener(this);

            mImage = (ImageView) itemView.findViewById(R.id.imageContact);
            mType = (TextView) itemView.findViewById(R.id.typeEvent);
            mTitle = (TextView) itemView.findViewById(R.id.titleEvent);
            mPeopleName = (TextView) itemView.findViewById(R.id.ministrante);
            mHour = (TextView) itemView.findViewById(R.id.horario);
        }

        @Override
        public void onClick(View view) {
            Event event = events.get(getPosition());
            new AlertDialog.Builder(context)
                    .setTitle(event.getTitle())
                    .setMessage(event.getPeople() + "\n\n" + event.getDescription() + "\n\n" +
                            "Local: " + event.getLocal() + "\n\nHorário: " + event.getHour())
                    .create().show();
        }
    }

}
