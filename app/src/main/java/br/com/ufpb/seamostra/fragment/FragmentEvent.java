package br.com.ufpb.seamostra.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.com.ufpb.seamostra.R;
import br.com.ufpb.seamostra.SeAmostraApplication;
import br.com.ufpb.seamostra.util.custom.EventsAdapter;

public class FragmentEvent extends Fragment {

    private int data;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        data = getArguments().getInt("date");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notebook, container, false);

        SeAmostraApplication application = (SeAmostraApplication) getActivity().getApplicationContext();

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(new EventsAdapter(getActivity(), application.getEventDay(data)));

        return view;
    }
}
