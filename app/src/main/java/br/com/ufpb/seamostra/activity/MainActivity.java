package br.com.ufpb.seamostra.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import java.util.HashMap;

import br.com.ufpb.seamostra.R;
import br.com.ufpb.seamostra.SeAmostraApplication;
import br.com.ufpb.seamostra.util.custom.ComissaoAdapter;

public class MainActivity extends AppCompatActivity {

    private Toolbar toolbar;

    private SeAmostraApplication application;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        application = (SeAmostraApplication) getApplicationContext();

        toolbar = (Toolbar) findViewById(R.id.toolbar_main);
        setSupportActionBar(toolbar);
        createSlideImage();

        createNavigationDrawer(savedInstanceState);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerConfirmed);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(new ComissaoAdapter(this, application.createConfirmed()));

        RecyclerView recyclerView2 = (RecyclerView) findViewById(R.id.recyclerComission);
        recyclerView2.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        recyclerView2.setItemAnimator(new DefaultItemAnimator());
        recyclerView2.setAdapter(new ComissaoAdapter(this, application.createComission()));

    }

    private Typeface getTypeface() {
        return Typeface.createFromAsset(getAssets(), "fonts/roboto_light.ttf");
    }

    private void createSlideImage() {
        SliderLayout mDemoSlider = (SliderLayout) findViewById(R.id.slider);
        HashMap<String, Integer> hashMap = createHash();
        for (String name : hashMap.keySet()) {
            TextSliderView sliderView = new TextSliderView(this);
            sliderView.image(hashMap.get(name))
                    .description(name)
                    .setScaleType(BaseSliderView.ScaleType.Fit);
            mDemoSlider.addSlider(sliderView);
        }
        mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Fade);
        mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        mDemoSlider.setCustomAnimation(new DescriptionAnimation());
        mDemoSlider.setDuration(3000);
    }

    private HashMap<String, Integer> createHash() {
        HashMap<String, Integer> hashMap = new HashMap<>();
        hashMap.put("Se Amostra 2016", R.drawable.seamostra2);
        hashMap.put("III Semana Academica de Design da UFPB", R.drawable.background3);
        hashMap.put("Subúrbya", R.drawable.suburbia);
        hashMap.put("Alice", R.drawable.alice);

        return hashMap;
    }

    private void createNavigationDrawer(Bundle savedInstanceState) {
        AccountHeader accountHeader = new AccountHeaderBuilder().withActivity(this)
                .withCompactStyle(false)
                .withTranslucentStatusBar(true)
                .withSavedInstance(savedInstanceState)
                .withTypeface(getTypeface())
                .withTextColor(getResources().getColor(R.color.md_black_1000))
                .withHeaderBackground(R.drawable.ico1)
                .build();

        new DrawerBuilder().withActivity(this)
                .withActionBarDrawerToggleAnimated(true)
                .withDrawerGravity(Gravity.START)
                .withToolbar(toolbar)
                .withSavedInstance(savedInstanceState)
                .withTranslucentStatusBar(true)
                .withAccountHeader(accountHeader)
                .withItemAnimator(new DefaultItemAnimator())
                .withSelectedItem(-1)
                .addDrawerItems(

                        new PrimaryDrawerItem().withName("Agenda")
                                .withIcon(R.drawable.ic_calendar)
                                .withTag("notbook"),

                        new PrimaryDrawerItem().withName("Mapa do Campus")
                                .withIcon(R.drawable.ic_location)
                                .withTag("map_campus"),

                        new PrimaryDrawerItem().withName("Mapa da Cidade")
                                .withIcon(R.drawable.ic_location)
                                .withTag("map_city"),

                        new PrimaryDrawerItem().withName("Sobre")
                                .withIcon(R.drawable.ic_about)
                                .withTag("about")

                ).withOnDrawerItemClickListener(new DrawerItemClickListener())
                .build();
    }

    private class DrawerItemClickListener implements Drawer.OnDrawerItemClickListener {

        @Override
        public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {

            String tag = drawerItem.getTag().toString();

            Intent intent;

            switch (tag) {
                case "notbook":
                    intent = new Intent(MainActivity.this, NotebookActivity.class);
                    startActivity(intent);
                    return true;
                case "map_campus":
                    intent = new Intent(MainActivity.this, MapCampusActivity.class);
                    startActivity(intent);
                    return false;
                case "map_city":
                    intent = new Intent(MainActivity.this, MapCityActivity.class);
                    startActivity(intent);
                    return false;
                case "about":
                    intent = new Intent(MainActivity.this, AboutActivity.class);
                    startActivity(intent);
                    return false;
            }

            return true;
        }
    }
}
