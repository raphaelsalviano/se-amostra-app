package br.com.ufpb.seamostra.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBCore extends SQLiteOpenHelper {

    private static final String NAME_DB = "SeAmostraDataBase";
    private static final int VERSION_DB = 21;
    private final DBCode code;

    public DBCore(Context context) {
        super(context, NAME_DB, null, VERSION_DB);
        code = new DBCode();
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        for (String s : code.createTables()){
            db.execSQL("CREATE TABLE " + s);
        }
        for (String s : code.insertsTable()){
            db.execSQL("INSERT INTO " + s);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("drop table event;");
        onCreate(db);
    }
}
